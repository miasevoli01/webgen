# WebGen
A little Python 3 script made for automating web design - version 1.0

## How It Works
The program requires an HTML file to act as a template. Within the template file, you should place comments where you intend to insert non-generic content. When the program is run, files are read from a specified input folder. All non-HTML files will be copied to a specified output folder. HTML files in this folder should contain only comments and the content that should be inserted into the template immediately following them. A new HTML file will be created in the output directory that contains the template along with the content replacing the matching HTML comments.

## Note for Windows users...
To use this program on a Windows computer, you need to install the win32api package via pip. Simply run the following in command prompt:
```
pip install pywin32
```

## Usage
```
usage: webgen.py [-i <input>] [-o <output>] [-t <template>] [-chHq]
	c : display copyright information
	h : display this help page
	H : copy hidden files in the input directory to the output directory
	q : used quiet mode (no terminal output)
	
	i path : use 'path' as the input directory
	o path : use 'path' as the output directory
	t file : use 'file' as the template file, by default this is the 'template.html' file in the input directory
```

## Example
Let's say you have a folder, called `input`, which contains `template.html` and `content.html`.
### template.html
```
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- Page info -->
	</head>
	
	<body>
		<header>This is my website!</header>
		<main>
			<!-- Page content -->
		</main>
	</body>
<html>
```
### content.html
```
<!-- Page info -->
<title>Test page</title>

<!-- Page content -->
<p>Here is some content</p>
```
Running `python3 webgen.py -i input -o output` would produce a directory called `output` with a single file, `content.html`.
### content.html
```
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Test page</title>
	</head>
	
	<body>
		<header>This is my website!</header>
		<main>
			<p>Here is some content</p>
		</main>
	</body>
<html>
```

## Purpose
The intention of this project is to make web development easier. Instead of having to manually update elments that are common to multiple pages of a website, such as the header and navigation, it can all be done consistently and easily through the help of this Python script.

## Author
This program was designed and authored by Matthew Iasevoli. This program is free software. For more information see the file "LICENSE.MD". I can be contacted at miasevoli@acm.org.
