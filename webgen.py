#!/usr/bin/env python3

"""
webgen.py - Version 1.0

Copyright (C) 2022 Matthew Iasevoli

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import getopt, shutil, sys, os
if os.name == 'nt':
    import win32api, win32con

# Default settings for output and hidden files
output_on = True
process_hidden = False

def main(argv, cmd):
    global output_on
    global process_hidden

    # Default directory locations
    input_dir = "content"
    output_dir = "website"
    global template_file
    template_file = ""
    
    # Check for errors in command arguments
    try:
        opts, args = getopt.getopt(argv,"chHi:o:qt:",["help"])
    except getopt.GetoptError as e:
        print(e.args[0])
        usage()
        print("try '" + cmd + " --help' for more information")
        sys.exit(2)

    # Process command arguments
    for opt, arg in opts:
        if opt in ("-h" "--help"):
            # Display help
            help()
            sys.exit()
        elif opt == "-c":
            # Display copyright information
            copyright()
            sys.exit()
        elif opt == "-H":
            # Process hidden files
            process_hidden = True
        elif opt == "-i":
            # Specify input directory
            input_dir = arg
        elif opt == "-o":
            # Specify output directory
            output_dir = arg
        elif opt == "-q":
            # Hide output (quiet mode)
            output_on = False
        elif opt == "-t":
            # Specify template file
            template_file = arg
    if template_file == "":
        template_file = os.path.join(input_dir, "template.html")

    # Find every file in the input directory and its subdirectories
    try:
        input_files = list()
        for root, dirs, files in os.walk(input_dir):
            for file in files:
                input_files.append(os.path.join(root, file))
    except Exception as e:
        console_output(e)
        console_output("Could not read the input directory")
        sys.exit(1)

    # Read the template file
    global template_lines
    global template_sections
    try:
        with open(template_file) as f:
            template_lines = f.readlines()
        # Create a set of sections where content can be inserted
        template_sections = set()
        for line in template_lines:
            if line.strip().startswith("<!--"):
                template_sections.add(line.strip())
    except Exception as e:
        console_output(e)
        console_output("Could not read the template file")
        sys.exit(1)

    # Process files
    success = True
    for file in input_files:
        if should_be_processed(file):
            try:
                new_file = os.path.join(output_dir, os.path.relpath(file, input_dir))
                os.makedirs(os.path.dirname(new_file), exist_ok=True)
                if file.endswith(".html") or file.endswith(".htm"):
                    # Apply template to HTML file
                    console_output("Using " + file + " to create " + new_file)
                    generate_html(file, new_file)
                else:
                    # Copy non-HTML file
                    shutil.copy(file, new_file)
                    console_output("Copying " + file + " to " + new_file)
            except Exception as e:
                console_output("There was a problem with " + file + ":")
                console_output(e)
                success = False
        else:
            console_output("Skipped " + file)

    # If there were errors, let the user know
    if success:
        sys.exit(0)
    else:
        console_output("Completed the operation with erros")
        sys.exit(1)


def console_output(msg):
    '''Displays a message if the user is not in quiet mode'''
    if output_on:
        print(msg)

def should_be_processed(file):
    '''Determines if a file needs to copied based on input parameters'''
    if file == template_file:
        return False
    elif process_hidden:
        return True
    elif os.name== 'nt':
        # Look at file attributes on Windows based systems
        attribute = win32api.GetFileAttributes(file)
        return not (attribute & (win32con.FILE_ATTRIBUTE_HIDDEN | win32con.FILE_ATTRIBUTE_SYSTEM))
    else:
        # Hidden files start with "." on Unix-like systems
        file = os.path.basename(file)
        return not file.startswith(".")

def generate_html(file, new_file):
    '''Create a new HTML file from a content file'''
    
    # Create a dictionary of sections and their content
    global template_sections
    content = {}
    current_section = ""
    current_section_content = []
    with open(file) as f:
        for line in f.readlines():
            stripped_line = line.strip()
            if stripped_line.startswith("<!--") and stripped_line in template_sections:
                if not current_section == "":
                    content[current_section] = current_section_content
                    current_section_content = []
                current_section = stripped_line
            else:
                current_section_content.append(line)
        content[current_section] = current_section_content

    # Place content from dictionary into new file
    global template_lines
    with open(new_file, "wt") as f:
        for line in template_lines:
            left_stripped_line = line.lstrip()
            stripped_line = line.strip()
            if stripped_line.startswith("<!--") and stripped_line in content:
                whitespace = line[0:-len(left_stripped_line)]
                for l in content[stripped_line]:
                    f.write(whitespace + l)
            else:
                f.write(line)


def usage():
    print("usage: webgen.py [-i <input>] [-o <output>] [-t <template>] [-chHq]")

def help():
    usage()
    print("""
    c : display copyright information
    h : display this help page
    H : copy hidden files in the input directory to the output directory
    q : used quiet mode (no terminal output)

    i path : use `path' as the input directory
    o path : use `path' as the output directory
    t file : use `file' as the template file, by default this is the `template.html' file in the input directory

    For more info, see the README.md file included with this program.""")

def copyright():
    with open(__file__) as f:
        f_lines = f.readlines()
        for line in f_lines[3:19]:
            print(line.rstrip())

if __name__ == "__main__":
    main(sys.argv[1:], sys.argv[0])
